# Atividade Prática

## Sprint 1

### Funcionamento do código calculadora.js

```bash
node calculadora.js <primeiro-termo> <segundo-termo>
```
### Funcionamento do código calculadora.js de João

```bash
node calculadora.js <primeiro-termo> <sinal-de-operação> <segundo-termo>
```

Esse código realiza qualquer operação que utiliza operadores binários, aritméticos ou de comparação.